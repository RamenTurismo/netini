﻿# NetIni

NetIni is a library to deserialize and serialize a `.ini` file type with .NET 5 and .NET Core 3+. 
The library is set with `netstandard2.1`.

It has been thought as to take the minimum required allocation in memory, to put less pressure on the GC, and to be fast to execute.

## Benchmarks

- [Benchmarks](benchmarks/)

## How to use

`IniDocument` is an object containing all deserialized properties, sections and comments.

### Deserialize the whole file to a Document

`IniDocument` represents the file.

To Deserialize a file :

```csharp
IniDocument document = IniSerializer.DeserializeFile("C:\\myFile.ini");

// Async version
IniDocument document = await IniSerializer.DeserializeFileAsync("C:\\myFile.ini");
```

To Deserialize a string :

Note that there is an implicit conversion from `string` to `ReadOnlySpan<char>`:

```csharp
IniDocument document = IniSerializer.Deserialize(myString);
```

Deserialize to a class 

```csharp
    IniConfigurationBuilder configuration = new();
            
    configuration.Configure(options =>
        {
            options.IncludeSections = true;
            options.IncludeComments = false;
        })
        .Map<MyIniFile>()
            .AddProperty(
                e => e.MySection!.Dollars,
                "Dollar",
                "MY_SECTION",
                new NumericIntConverter())
            .AddProperty(
                e => e.MySection!.IsEmpty,
                "Empty",
                "MY_SECTION",
                new NumericBoolConverter());

    MyIniFile deserialized = IniSerializer.Deserialize<MyIniFile>(
        _fileString,
        configuration.Build());
```

### Serialize document to file

To serialize a string :

```csharp
var iniDocument = new IniDocument();
// stuff
string myDocument = IniSerializer.Serialize(iniDocument);
```

To serialize to a file :

```csharp
var iniDocument = new IniDocument();
// stuff
IniSerializer.Serialize(iniDocument, "C:/path/to/file.ini");
```