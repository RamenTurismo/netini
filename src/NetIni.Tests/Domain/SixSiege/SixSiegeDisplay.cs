﻿using NetIni.Attributes;

namespace NetIni.Tests.Domain.SixSiege
{
    [IniSection("DISPLAY_SETTINGS")]
    public sealed class SixSiegeDisplay
    {
        [IniProperty("GPUAdapter")]
        public int GpuAdapter { get; set; }

        [IniProperty("Monitor")]
        public int Monitor { get; set; }

        [IniProperty("ResolutionWidth")]
        public int ResolutionWidth { get; set; }

        [IniProperty("ResolutionHeight")]
        public int ResolutionHeight { get; set; }
        
        public bool WindowMode { get; set; }

        [IniProperty("VSync")]
        public bool VSync { get; set; }

        [IniProperty("RefreshRate")]
        public double RefreshRate { get; set; }
    }
}
