﻿using System.IO;
using System.Threading.Tasks;
using FluentAssertions;
using NetIni.Configuration;
using NetIni.Converters;
using NetIni.Tests.Domain.SixSiege;
using Xunit;

namespace NetIni.Tests.Deserialization
{
    [Collection(nameof(IniDeserializerFixture))]
    public class IniDeserializerTests
    {
        private readonly IniDeserializerFixture _fixture;

        public IniDeserializerTests(IniDeserializerFixture fixture)
        {
            _fixture = fixture;
            _fixture.ResetStreams();
        }

        [Fact]
        public void ReadIniDocumentFromString()
        {
            var streamReader = new StreamReader(_fixture.FileWithSections);

            IniDocument parsed = IniSerializer.Deserialize(streamReader.ReadToEnd());
            
            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public void ReadIniDocumentFromStreamWithEndOfLine()
        {
            IniDocument parsed = IniSerializer.Deserialize(_fixture.FileWithSectionsNewLineAtEnd);

            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public void ReadIniDocumentFromStream()
        {
            IniDocument parsed = IniSerializer.Deserialize(_fixture.FileWithSections);

            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public void ReadIniDocumentFromStreamFilePath()
        {
            IniDocument parsed = IniSerializer.DeserializeFile(_fixture.FileWithSections.Name);

            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public async Task ReadIniDocumentFromStreamAsync()
        {
            IniDocument parsed = await IniSerializer.DeserializeAsync(_fixture.FileWithSections);

            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public async Task ReadIniDocumentFromStreamFilePathAsync()
        {
            IniDocument parsed = await IniSerializer.DeserializeFileAsync(_fixture.FileWithSections.Name);

            IniDeserializerFixture.AssertWholeIniDocument(parsed);
        }

        [Fact]
        public void ReturnNullWhenStreamPositionAtEnd()
        {
            _fixture.FileWithSections.Position = _fixture.FileWithSections.Length;

            IniDocument parsed = IniSerializer.Deserialize(_fixture.FileWithSections);

            parsed.IsEmpty.Should().BeTrue("parsed.IsEmpty");
        }

        [Fact]
        public void DeserializeToObjectAndConvertsValues()
        {
            IniConfigurationBuilder configuration = new();
            
            configuration.Configure(options =>
                {
                    options.IncludeSections = false;
                    options.IncludeComments = false;
                })
                .Map<SixSiegeConfiguration>()
                    .AddProperty(
                        e => e.DisplaySettings!.GpuAdapter,
                        "GPUAdapter",
                        "DISPLAY_SETTINGS",
                        new NumericIntConverter())
                    .AddProperty(
                        e => e.DisplaySettings!.WindowMode,
                        "WindowMode",
                        "DISPLAY_SETTINGS",
                        new NumericBoolConverter());

            SixSiegeConfiguration deserialized = IniSerializer.Deserialize<SixSiegeConfiguration>(
                _fixture.FileWithSections,
                configuration.Build());

            deserialized.DisplaySettings.Should().NotBeNull();
            deserialized.DisplaySettings?.GpuAdapter.Should().Be(3);
            deserialized.DisplaySettings?.WindowMode.Should().Be(true);
        }
    }
}
