﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace NetIni.Tests.Deserialization
{
    [CollectionDefinition(nameof(IniDeserializerFixture))]
    public class IniDeserializerFixture : IDisposable, ICollectionFixture<IniDeserializerFixture>
    {
        public FileStream FileWithSections { get; }

        public FileStream FileWithSectionsNewLineAtEnd { get; }

        public IniDeserializerFixture()
        {
            FileWithSections = GetFileStream("FileWithSections.ini");
            FileWithSectionsNewLineAtEnd = GetFileStream("FileWithSectionsNewLineAtEnd.ini");
        }

        public void ResetStreams()
        {
            FileWithSections.Position = 0;
            FileWithSectionsNewLineAtEnd.Position = 0;
        }

        /// <inheritdoc />
        public void Dispose()
        {
            FileWithSections.Dispose();
            FileWithSectionsNewLineAtEnd.Dispose();
        }

        public static void AssertWholeIniDocument(IniDocument parsed)
        {
            parsed.Sections.Should().HaveCount(10);

            parsed.Sections.Should().SatisfyRespectively(
                first =>
                {
                    first.Name.Should().Be("DISPLAY_SETTINGS");
                },
                second =>
                {
                    second.Name.Should().Be("HARDWARE_INFO");
                },
                third =>
                {
                    third.Name.Should().Be("QUALITY_SETTINGS");
                },
                fourth =>
                {
                    fourth.Name.Should().Be("CUSTOM_PRESET_SETTINGS");
                },
                fifth =>
                {
                    fifth.Name.Should().Be("READ_ONLY");
                },
                sixth =>
                {
                    sixth.Name.Should().Be("INPUT");
                },
                seventh =>
                {
                    seventh.Name.Should().Be("DISPLAY");
                },
                eigth =>
                {
                    eigth.Name.Should().Be("AUDIO");
                },
                ninth =>
                {
                    ninth.Name.Should().Be("GAMEPLAY");
                },
                tenth =>
                {
                    tenth.Name.Should().Be("ONLINE");

                    tenth.Comments.Should().HaveCount(13);
                    tenth.Properties.Should().HaveCount(2);

                    tenth.Properties.Should().SatisfyRespectively(
                        dataCenterProperty =>
                        {
                            dataCenterProperty.Key.Should().Be("DataCenterHint");
                            dataCenterProperty.Value.Should().Be("default");
                        },
                        useUpnpProperty =>
                        {
                            useUpnpProperty.Key.Should().Be("UseUPnP");
                            useUpnpProperty.Value.Should().Be("1");
                        });
                });
        }

        private static FileStream GetFileStream(string fileName)
        {
            return TestHelper.GetFileStream(Path.Combine(Directory.GetCurrentDirectory(), "Deserialization", "Files", fileName));
        }
    }
}
