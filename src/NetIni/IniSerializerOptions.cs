﻿using System;
using System.Text;

namespace NetIni
{
    public sealed class IniSerializerOptions
    {
        public static readonly IniSerializerOptions Default = new()
        {
            IncludeComments = true,
            IncludeSections = true
        };

        public IniSerializerOptions()
        {
            Encoding = Encoding.UTF8;
            BufferSize = 4096;
            NewLine = Environment.NewLine;
            CommentDelimiter = ';';
        }
        
        /// <summary>
        /// Should the deserializer include comments.
        /// </summary>
        public bool IncludeComments { get; set; }

        /// <summary>
        /// Should the deserializer include sections.
        /// <para></para>
        /// If <c>false</c>, Properties will be put at the root of the document.
        /// </summary>
        public bool IncludeSections { get; set; }

        /// <summary>
        /// Used encoding to read or write to files.
        /// <para></para>
        /// Default is <c>UTF-8</c>.
        /// </summary>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Stream buffer size to read or write.
        /// <para></para>
        /// Default is <c>4096</c>.
        /// </summary>
        public int BufferSize { get; set; }

        /// <summary>
        /// Default is environment's default.
        /// </summary>
        public string NewLine { get; set; }

        /// <summary>
        /// Default is <c>;</c>.
        /// </summary>
        public char CommentDelimiter { get; set; }
    }
}
