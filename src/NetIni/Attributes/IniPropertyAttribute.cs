﻿using System;

namespace NetIni.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class IniPropertyAttribute : Attribute
    {
        public string Key { get; }

        /// <inheritdoc />
        public IniPropertyAttribute(string key)
        {
            Key = key;
        }
    }
}
