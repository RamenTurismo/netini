﻿using System;

namespace NetIni.Configuration
{
    public interface IConfiguration
    {
        IniSerializerOptions? Options { get; }

        ITypeConfiguration Get(Type type);
    }
}
