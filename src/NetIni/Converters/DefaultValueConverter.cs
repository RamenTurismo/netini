﻿using System;
using System.Diagnostics;
using System.Reflection;

namespace NetIni.Converters
{
    [DebuggerDisplay("string → object")]
    internal class DefaultValueConverter : ValueConverter<object>
    {
        /// <inheritdoc />
        public override object? Convert(PropertyInfo propertyInfo, in ReadOnlySpan<char> value)
        {
            return System.Convert.ChangeType(value.ToString(), propertyInfo.PropertyType);
        }

        /// <inheritdoc />
        public override ReadOnlySpan<char> ConvertBack(in object? value)
        {
            if (value == null)
            {
                return ReadOnlySpan<char>.Empty;
            }

            return value.ToString().AsSpan();
        }
    }
}
