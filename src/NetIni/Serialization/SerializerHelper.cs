﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetIni.Serialization
{
    internal static class SerializerHelper
    {
        internal static string GetStringBuilder(this IniDocument document, IniSerializerOptions options)
        {
            var stringBuilder = new StringBuilder();

            foreach (string comment in document.Comments)
            {
                stringBuilder.AppendComment(options.CommentDelimiter, comment);
            }

            int currentSectionIndex = 0;
            int totalSectionIndexes = document.Sections.Count - 1;

            foreach (IniSection section in document.Sections)
            {
                // Name.
                stringBuilder.Append(IniParserConsts.SectionNameStart)
                    .Append(section.Name)
                    .Append(IniParserConsts.SectionNameEnd)
                    .AppendLine();

                // Comments.
                foreach (string sectionComment in section.Comments)
                {
                    stringBuilder.AppendComment(options.CommentDelimiter, sectionComment);
                }

                // Properties.
                if (section.Properties.Count > 1)
                {
                    stringBuilder.AppendProperties(section.Properties);
                }
                else if (section.Properties.Count == 1)
                {
                    stringBuilder.AppendLine(section.Value);
                }

                if (currentSectionIndex < totalSectionIndexes)
                {
                    stringBuilder.AppendLine();
                }

                currentSectionIndex++;
            }

            stringBuilder.AppendProperties(document.Properties);

            return stringBuilder.ToString();
        }

        internal static Memory<byte> GetBytes(IniDocument document, Encoding encoding, IniSerializerOptions options)
        {
            ReadOnlySpan<char> iniFileContent = document.GetStringBuilder(options).AsSpan();

            int byteCount = encoding.GetByteCount(iniFileContent);
            byte[] bytes = new byte[byteCount];

            Span<byte> bytesView = bytes.AsSpan();

            options.Encoding.GetBytes(iniFileContent, bytesView);

            return bytes.AsMemory();
        }

        internal static FileStream GetWriteFileStream(in string filePath, in int bufferSize)
        {
            return new(filePath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite, bufferSize);
        }

        private static StringBuilder AppendComment(this StringBuilder stringBuilder, in char commentDelimiter, in string comment)
        {
            return stringBuilder.Append(commentDelimiter).AppendLine(comment);
        }

        private static StringBuilder AppendProperties(this StringBuilder stringBuilder, IEnumerable<IniProperty> properties)
        {
            foreach (IniProperty sectionProperty in properties)
            {
                stringBuilder.AppendLine(sectionProperty.KeyValue);
            }

            return stringBuilder;
        }
    }
}
