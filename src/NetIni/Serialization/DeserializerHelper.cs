﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace NetIni.Serialization
{
    internal static class DeserializerHelper
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static void Build(
            IniDocument iniDocument,
            IniSection? currentSection,
            in ReadOnlySpan<char> view,
            IniSerializerOptions options,
            out IniSection? section)
        {
            section = currentSection;

            // Empty line happens in those files.
            if (view.Length == 0)
            {
                return;
            }

            if (view[0] == options.CommentDelimiter)
            {
                if (!options.IncludeComments)
                {
                    return;
                }

                ICollection<string> comments;

                if (section != null)
                {
                    comments = section.Comments;
                }
                else
                {
                    comments = iniDocument.Comments;
                }

                comments.Add(view[1..].ToString());

                return;
            }

            if (view[0] == IniParserConsts.SectionNameStart && view[^1] == IniParserConsts.SectionNameEnd)
            {
                if (!options.IncludeSections)
                {
                    return;
                }

                section = new IniSection(view[1..^1].ToString());

                iniDocument.Sections.Add(section);

                return;
            }

            IniProperty property = GetProperty(view);

            if (currentSection != null)
            {
                currentSection.Properties.Add(property);
            }
            else
            {
                iniDocument.Properties.Add(property);
            }
        }

        internal static FileStream GetReadonlyFileStream(in string filePath, in int bufferSize)
        {
            // ReadWrite for concurrency access.
            return new(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, bufferSize);
        }

        internal static async Task<IniDocument> DeserializeAsync(IniDocument iniDocument, Stream stream, IniSerializerOptions options)
        {
            IniSection? currentSection = null;

            var reader = new StreamReader(stream, options.Encoding, false);

            string? line;

            while ((line = await reader.ReadLineAsync()) != null)
            {
                Build(iniDocument, currentSection, line.AsSpan(), options, out currentSection);
            }

            return iniDocument;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static IniProperty GetProperty(in ReadOnlySpan<char> view)
        {
            int indexOfSeparator = view.IndexOf(IniParserConsts.KeyValueSeparator);

            if (indexOfSeparator != -1)
            {
                return new IniProperty(view[..indexOfSeparator].ToString(), view[(indexOfSeparator + 1)..].ToString());
            }

            return new IniProperty(view.ToString());
        }
    }
}
