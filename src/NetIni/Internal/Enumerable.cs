﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NetIni.Internal
{
    internal static class Enumerable
    {
        /// <summary>
        /// NetIni.Any optimized to avoid allocation from Linq.Any().
        /// </summary>
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        internal static bool NAny<T>(this ICollection<T> collection)
        {
            return collection.Count > 1;
        }
    }
}
