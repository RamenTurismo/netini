﻿using System;
using System.Collections.Generic;
using System.Linq;
using NetIni.Internal;

namespace NetIni
{
    public sealed record IniDocument
    {
        public ICollection<IniProperty> Properties { get; }

        public ICollection<IniSection> Sections { get; }

        public ICollection<string> Comments { get; }

        public bool IsEmpty => !Properties.NAny() && !Sections.NAny() && !Comments.NAny();
        
        public IniDocument()
        {
            Properties = new List<IniProperty>();
            Sections = new List<IniSection>();
            Comments = new List<string>();
        }

        /// <summary>
        /// Finds the property in <see cref="Properties"/> or <see cref="Sections"/> if not found.
        /// </summary>
        /// <returns><c>null</c> if not found.</returns>
        public IniProperty? FindProperty(Func<IniProperty, bool> predicate)
        {
            // Search in properties.
            IniProperty iniProperty = Properties.FirstOrDefault(predicate);

            if (iniProperty != default)
            {
                return iniProperty;
            }

            // Search in sections otherwise.
            return Sections.SelectMany(e => e.Properties)
                .FirstOrDefault(predicate)
                .NullOrValue();
        }

        /// <inheritdoc />
        public override string ToString()
        {
            return $"{nameof(Properties)}: {Properties.Count}, {nameof(Sections)}: {Sections.Count}, {nameof(Comments)}: {Comments.Count}";
        }
    }
}
